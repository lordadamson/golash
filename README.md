# Gollash
A visual shell language, and a pipe multiplexer. The idea is to visually create nodes and connect them together. So you connect stdin, stdout and stderr visually. Unlike text you are not limited to pipe to only one process.

<img src="q1416Z.gif">

https://www.youtube.com/watch?v=EUqnxajlmkw