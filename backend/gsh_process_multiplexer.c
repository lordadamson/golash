#include "gsh_process_multiplexer.h"

#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

static int
is_within(int fd, int* arr, size_t arr_count)
{
    for(int i = 0; i < arr_count; i++)
    {
        if(fd == arr[i])
        {
            return 1;
        }
    }

    return 0;
}

static int
fd_is_valid(int fd)
{
    return fcntl(fd, F_GETFD) != -1 || errno != EBADF;
}

static void
close_all_files()
{
    const int max_fd = getdtablesize();

    for (int fd = 3; fd <= max_fd; fd++)
    {
        if(fd_is_valid(fd))
        {
            close(fd);
        }
    }
}

static void
close_all_files_except(int* exceptions, size_t exceptions_count)
{
    const int max_fd = getdtablesize();

    for (int fd = 3; fd <= max_fd; fd++)
    {
        if(fd_is_valid(fd) && !is_within(fd, exceptions, exceptions_count))
        {
            close(fd);
        }
    }
}

static void
close_files(int* files, size_t count)
{
    for(size_t i = 0; i < count; i++)
    {
        close(files[i]);
    }
}

struct gsh_error_t
gsh_execute(struct gsh_pipeline_t* pipeline)
{
    struct gsh_error_t e = {0};

    int pid = fork();

    if(pid != 0)
    {
        int status;
        waitpid(pid, &status, WNOHANG);
        return e;
    }

    close_all_files();
    int pipes[MAX_CONNECTIONS][2];

    for(size_t i = 0; i < pipeline->connections_count; i++)
    {
        pipe(pipes[i]);
    }

    for(size_t i = 0; i < pipeline->commands_count; i++)
    {
        pid = fork();

        if(pid != 0)
        {
            int status;
            waitpid(pid, &status, WNOHANG);
            continue;
        }

        int files_used[MAX_CONNECTIONS] = {0};
        size_t files_used_count = 0;

        for(size_t j = 0; j < pipeline->connections_count; j++)
        {
            struct gsh_connection_t c = pipeline->connections[j];

            if(c.from_process == i)
            {
                files_used[files_used_count++] = pipes[j][1];
            }

            if(c.to_process == i)
            {
                files_used[files_used_count++] = pipes[j][0];
            }
        }

        close_all_files_except(files_used, files_used_count);

        for(size_t j = 0; j < pipeline->connections_count; j++)
        {
            struct gsh_connection_t c = pipeline->connections[j];

            if(c.from_process == i)
            {
                dup2(pipes[j][1], c.from_port);
            }

            if(c.to_process == i)
            {
                dup2(pipes[j][0], 0);
            }
        }

        close_files(files_used, files_used_count);
        char* argv[] = {"sh", "-c", pipeline->commands[i].command, NULL};
        execvp(argv[0], argv);
    }
}

int
gsh_read(gsh_file file, int how_much, bool block)
{
    return 0;
}

void
gsh_pipeline_command_push(struct gsh_pipeline_t* pipeline, struct gsh_process_t command)
{
    pipeline->commands[pipeline->commands_count++] = command;
}

void
gsh_pipeline_connections_push(struct gsh_pipeline_t* pipeline, struct gsh_connection_t connection)
{
    pipeline->connections[pipeline->connections_count++] = connection;
}
