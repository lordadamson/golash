#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_COMMANDS 512
#define MAX_CONNECTIONS 512
#define MAX_COMMAND_LENGTH 1024
#define MAX_ERROR_LENGTH 1024

#include <stddef.h>
#include <stdbool.h>

/// gsh stands for gollash
typedef int gsh_file;

struct gsh_process_t
{
    char command[MAX_COMMAND_LENGTH];
};

struct gsh_connection_t
{
    /// specify the index of the process where the connection
    /// originates
    int from_process;

    /// specify the index of the process where the connection
    /// is going to
    /// put it as -1 in case the destination is a file on disk
    /// or any other file not related to a process
    int to_process;

    /// 0 => stdin
    /// 1 => stdout
    /// 2 => stderr
    gsh_file from_port;

    /// in case the destination was another process in the pipeline
    /// valid values would be:
    /// 0 => stdin
    /// 1 => stdout
    /// 2 => stderr
    ///
    /// You can also use gsh_open_file to open a file on disk
    /// and pass it here, and it will be written to upon execution
    ///
    /// The last valid value is -1
    /// If you pass -1 we'll replace the it with a valid
    /// file and have the process output write to it. Then
    /// you can read from it using gsh_read() to read from
    /// and do withever you need (display to the user or something idk)
    gsh_file to_port;
};

struct gsh_pipeline_t
{
    struct gsh_process_t commands[MAX_COMMANDS];
    struct gsh_connection_t connections[MAX_CONNECTIONS];
    size_t commands_count;
    size_t connections_count;
};

void
gsh_pipeline_command_push(struct gsh_pipeline_t* pipeline, struct gsh_process_t command);

void
gsh_pipeline_connections_push(struct gsh_pipeline_t* pipeline, struct gsh_connection_t connection);

struct gsh_error_t
{
    /// 0 in case of success
    int code;

    /// will contain a meaningful error in case of failure
    const char what[MAX_ERROR_LENGTH];
};

struct gsh_error_t
gsh_execute(struct gsh_pipeline_t* pipeline);


gsh_file
gsh_file_open(const char* path);

/// @param file       valid gsh_file openned using gsh_open_file or a port
///                   openned by a connection (see gsh_connection_t::to_port)
/// @param how_much   how many bytes to read?
/// @param block      specifies whether this function should block or return immediately
///                   careful for deadlocks if you choose to have this as true
/// @returns          number of bytes read. or a negative value in case of error
int
gsh_read(gsh_file file, int how_much, bool block);

#ifdef __cplusplus
} // extern "C"
#endif
