#include "connection.hpp"

Connection::Connection(QObject* parent) : QObject(parent)
{

}

int Connection::from_process()
{
    return m_from_process;
}
void Connection::set_from_process(int p)
{
    m_from_process = p;
}

int Connection::to_process()
{
    return m_to_process;
}

void Connection::set_to_process(int p)
{
    m_to_process = p;
}

int Connection::from_file()
{
    return m_from_file;
}

void Connection::set_from_file(int p)
{
    m_from_file = p;
}

int Connection::to_file()
{
    return m_to_file;
}

void Connection::set_to_file(int p)
{
    m_to_file = p;
}
