#pragma once

#include <QObject>
#include <QString>
#include <qqml.h>

class Connection : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int from_process READ from_process WRITE set_from_process NOTIFY from_process_changed FINAL)
    Q_PROPERTY(int to_process   READ to_process   WRITE set_to_process   NOTIFY to_process_changed   FINAL)
    Q_PROPERTY(int from_file    READ from_file    WRITE set_from_file    NOTIFY from_file_changed    FINAL)
    Q_PROPERTY(int to_file      READ to_file      WRITE set_to_file      NOTIFY to_file_changed      FINAL)
    QML_ELEMENT

    int m_from_process;
    int m_to_process;
    int m_from_file;
    int m_to_file;

public:
    explicit Connection(QObject *parent = nullptr);

    int from_process();
    void set_from_process(int);
    int to_process();
    void set_to_process(int);
    int from_file();
    void set_from_file(int);
    int to_file();
    void set_to_file(int);

signals:
    void from_process_changed();
    void to_process_changed();
    void from_file_changed();
    void to_file_changed();
};
