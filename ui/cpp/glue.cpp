#include "glue.hpp"

#include <gsh_process_multiplexer.h>

#include <string.h>

Backend::Backend(QObject *parent) :
    QObject(parent)
{
}

void Backend::execute(const QStringList& commands, const QVariantList& connections)
{
    gsh_pipeline_t pipeline = {0};

    if(commands.empty())
    {
        return;
    }

    for(size_t i = 0; i < commands.size(); i++)
    {
        gsh_process_t c{};

        strncpy(c.command, commands[i].toStdString().c_str(), commands[i].size());
        gsh_pipeline_command_push(&pipeline, c);
    }

    for(size_t i = 0; i < connections.size(); i++)
    {
        gsh_connection_t c{};

        c.from_process = connections[i].toMap()["from_process"].toInt();
        c.to_process = connections[i].toMap()["to_process"].toInt();
        c.from_port = connections[i].toMap()["from_port"].toInt();
        c.to_port = connections[i].toMap()["to_port"].toInt();

        gsh_pipeline_connections_push(&pipeline, c);
    }

    gsh_execute(&pipeline);
}
