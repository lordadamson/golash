#pragma once

#include <QObject>
#include <QString>
#include <qqml.h>

#include "connection.hpp"

class Backend : public QObject
{
    Q_OBJECT
    QML_ELEMENT

public:
    explicit Backend(QObject *parent = nullptr);

    Q_INVOKABLE void execute(const QStringList& commands, const QVariantList &connections);
};
