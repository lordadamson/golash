#include <QGuiApplication>
#include <QQuickView>
#include <QQmlApplicationEngine>
#include <QFileSystemWatcher>
#include <QFile>
#include <QDebug>
#include <QQmlContext>

#include "glue.hpp"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    Backend backend;

    QGuiApplication app(argc, argv);

    QQuickView qview;

    QFileSystemWatcher watcher;
    watcher.addPaths({
        SOURCE_DIR "qml/main.qml",
        SOURCE_DIR "qml/Node.qml",
        SOURCE_DIR "qml/NodeTitleBar.qml",
        SOURCE_DIR "qml/PortIn.qml",
        SOURCE_DIR "qml/PortInTextInput.qml",
        SOURCE_DIR "qml/PortOut.qml",
        SOURCE_DIR "qml/TopBar.qml",
        SOURCE_DIR "qml/Port.qml",
        SOURCE_DIR "qml/ConnectionAwi.qml",
        SOURCE_DIR "js/nodes.js",
        SOURCE_DIR "js/painting.js",
    });

    const QUrl url(QStringLiteral(SOURCE_DIR "qml/main.qml"));

    qview.engine()->rootContext()->setContextProperty("backend", &backend);
    qview.setResizeMode(QQuickView::ResizeMode::SizeRootObjectToView);
    qview.setSource(url);
    qview.show();

    QObject::connect(&watcher, &QFileSystemWatcher::fileChanged, &qview,
        [&qview, &url, &watcher] (const QString &path) {
            qview.engine()->clearComponentCache();
            qview.setSource(url);

            bool in_watcher = watcher.files().contains(path);
            if((!in_watcher) && QFile::exists(path))
            {
                watcher.addPath(path);
            }
    });

    return app.exec();
}
