let nodes = [];
let connections = [];
let commands = [];
var device_pixel_ratio = 1;

function on_port_clicked(p) {
    canvas.currentPort = p;
}

function finish_creation(com, x, y)
{
    if (com.status === Component.Ready)
    {
        let node = com.createObject(paint_area, {x:x, y:y, node_id: nodes.length});
        if (node === null)
        {
            console.log("Error creating object");
        }

        node.portClicked.connect(on_port_clicked);
        nodes.push(node);
    }
    else if (com.status === Component.Error)
    {
        console.log("Error loading component:", com.errorString());
    }
}

function create_node(x, y)
{
    let com = Qt.createComponent("../qml/Node.qml");

    if (com.status === Component.Ready)
    {
        finish_creation(com, x, y);
    }
    else
    {
        com.statusChanged.connect(finish_creation, com, x, y);
    }
}

function is_port_hovered(canvas, mouse, port)
{
    let coords = port.mapToGlobal(port.x, port.y);

    let portx = coords.x;
    let porty = coords.y;
    let portw = port.circle.portmouse.width*device_pixel_ratio*1.3;
    let porth = port.circle.portmouse.height*device_pixel_ratio*1.3;

    if(mouse.x < portx)
    {
        return false;
    }

    if(mouse.y < porty)
    {
        return false;
    }

    if(mouse.x > portx + portw)
    {
        return false;
    }

    if(mouse.y > porty + porth)
    {
        return false;
    }

    return true;
}

function connect_ports(canvas, mouse, current_port)
{
    if(current_port === null)
    {
        return;
    }

    for(let i = 0; i < nodes.length; i++)
    {
        if(is_port_hovered(canvas, mouse, nodes[i].stdin_port))
        {
            let c = {
                from_process: current_port.node_id,
                to_process: i,
                from_port: current_port.type === "stdout" ? 1 : 2,
                to_port: 0
            };
            connections.push(c);
            return true;
        }

        if(is_port_hovered(canvas, mouse, nodes[i].stdout_port))
        {
            return false;
        }

        if(is_port_hovered(canvas, mouse, nodes[i].stderr_port))
        {
            return false;
        }

        if(is_port_hovered(canvas, mouse, nodes[i].command_port))
        {
            connections.push({from: current_port, to: nodes[i].command_port});
            return true;
        }
    }

    return false;
}
