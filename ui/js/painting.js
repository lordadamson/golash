function draw_connection(ctx, fromx, fromy, tox, toy)
{
    ctx.lineWidth = 5
    ctx.strokeStyle = '#eee'

    let cx1 = fromx
    let cy1 = fromy
    let cx4 = tox
    let cy4 = toy
    let cx3 = cx1
    let cy3 = cy4
    let cx2 = cx4
    let cy2 = cy1

    ctx.beginPath()
    ctx.moveTo(cx1, cy1)

    for(let t = 0.0; t < 1.0; t += 0.01)
    {
        let x = (1-t)*(1-t)*(1-t)*cx1 + 3*(1-t)*(1-t)*t*cx2 + 3*(1-t)*t*t*cx3 + t*t*t*cx4
        let y = (1-t)*(1-t)*(1-t)*cy1 + 3*(1-t)*(1-t)*t*cy2 + 3*(1-t)*t*t*cy3 + t*t*t*cy4
        ctx.lineTo(x, y)
    }

    ctx.lineTo(cx4, cy4)
    ctx.stroke()
}

function paint(canvas, nodes, connections)
{
    let ctx = canvas.getContext("2d")
    let paint_area = canvas.paint_area

    ctx.fillStyle = '#333'
    ctx.fillRect(0, 0, width, height)

    for(let i = 0; i < connections.length; i++)
    {
        let c = connections[i];
        let from = c.from_process;
        let to = c.to_process;

        let from_node = nodes[from];
        let to_node = nodes[to];

        let from_port = c.from_port === 1 ? from_node.stdout_port : from_node.stderr_port;
        let to_port = to_node.stdin_port;

        let from_coords = from_port.mapToItem(canvas, from_port.width/2, from_port.height/2);
        let to_coords = to_port.mapToItem(canvas, to_port.width/2, to_port.height/2);

        draw_connection(ctx, from_coords.x, from_coords.y, to_coords.x, to_coords.y);
    }


    if(!canvas.currentPort)
    {
        return
    }

    draw_connection(ctx, canvas.lastX, canvas.lastY, paint_area.mouseX, paint_area.mouseY);
}
