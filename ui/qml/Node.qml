import QtQuick
import QtQuick.Layouts
import QtQuick.Controls

ColumnLayout {
    id: node
    spacing: 0
    width: 200
    height: 100
    property alias stdin_port: stdin_port
    property alias stdout_port: stdout_port
    property alias stderr_port: stderr_port
    property alias command_port: command_port
    property alias command_text: command_port.textvalue
    property int node_id: 0

    signal portClicked(port: MouseArea)

    Connections {
        target: stdin_port.circle.portmouse
        function onPortClicked(p) {
            portClicked(p)
        }
    }

    Connections {
        target: command_port.circle.portmouse
        function onPortClicked(p) {
            portClicked(p)
        }
    }

    Connections {
        target: stdout_port.circle.portmouse
        function onPortClicked(p) {
            portClicked(p)
        }
    }

    Connections {
        target: stderr_port.circle.portmouse
        function onPortClicked(p) {
            portClicked(p)
        }
    }

    NodeTitleBar {
        nodetitle.text: {
            command_port.textvalue.split(' ')[0]
        }

        DragHandler {
            target: parent.parent
        }
    }

    // body
    Rectangle {
        width: parent.width
        height: parent.height

        RowLayout {
            anchors.fill: parent
            spacing: 0

            // process input
            ColumnLayout {
                Layout.leftMargin: -stdin_port.circle.implicitWidth/2
                Layout.fillHeight: true
                spacing: 0

                PortIn {
                    id: stdin_port
                    label: 'stdin'
                    type: stdin_port.label
                    node_id: node.node_id
                }

                PortInTextInput {
                    id: command_port
                    textvalue: 'command'
                    node_id: node.node_id
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }

            // process output
            ColumnLayout {
                Layout.rightMargin: -stdout_port.circle.implicitWidth/2
                layoutDirection: Qt.RightToLeft
                Layout.fillHeight: true
                spacing: 0

                PortOut {
                    id: stdout_port
                    label: 'stdout'
                    type: stdout_port.label
                    node_id: node.node_id
                }

                PortOut {
                    id: stderr_port
                    label: 'stderr'
                    type: stderr_port.label
                    node_id: node.node_id
                }
            }
        }
    }
}
