import QtQuick

Rectangle {
    width: parent.width
    height: 20
    color: '#ccc'
    property alias nodetitle: nodetitletext
    Text {
        id: nodetitletext
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
