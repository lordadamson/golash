import QtQuick

Rectangle {
    property alias node_id: portmousearea.node_id
    property alias portmouse: portmousearea
    property alias type: portmousearea.type
    implicitWidth: 12
    implicitHeight: implicitWidth
    radius: 12
    color: '#349beb'

    MouseArea {
        id: portmousearea
        property int node_id: 0
        property string type: ""
        hoverEnabled: true
        anchors.fill: parent
        signal portClicked(port: MouseArea)
        onPressed: (mouse) => {
            mouse.accepted = false
            portClicked(portmousearea)
        }
    }
}

