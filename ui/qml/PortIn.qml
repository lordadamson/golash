import QtQuick
import QtQuick.Layouts

Item
{
    property alias node_id: circle.node_id
    implicitWidth: portLayout.width
    implicitHeight: implicitHeight
    Layout.fillHeight: true
    property alias label: labeltext.text
    property alias type: circle.type
    property alias circle: circle

    RowLayout
    {
        id: portLayout
        anchors.centerIn: parent

        Port {
            id: circle
        }

        Text {
            id: labeltext
            text: 'ay 7aga'
        }
    }
}
