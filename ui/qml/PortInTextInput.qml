import QtQuick
import QtQuick.Layouts

Item
{
    property alias node_id: circle.node_id
    implicitWidth: portLayout.width
    implicitHeight: implicitHeight
    Layout.fillHeight: true
    property alias textvalue: labeltext.text
    property alias circle: circle

    RowLayout
    {
        id: portLayout
        anchors.centerIn: parent

        Port {
            id: circle
        }

        TextEdit {
            id: labeltext
            selectByMouse: true
            text: 'ay 7aga'
        }
    }

}
