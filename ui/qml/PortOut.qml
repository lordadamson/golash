import QtQuick
import QtQuick.Layouts

Item
{
    property alias node_id: circle.node_id
    implicitWidth: portLayout.width
    implicitHeight: implicitWidth
    Layout.fillHeight: true
    property alias label: labeltext.text
    property alias circle: circle
    property alias type: circle.type

    RowLayout
    {
        id: portLayout
        anchors.centerIn: parent

        Text {
            id: labeltext
            text: 'ay 7aga'
        }

        Port {
            id: circle
            color: '#8bbf4e'
        }
    }
}
