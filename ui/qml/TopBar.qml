import QtQuick
import QtQuick.Layouts

Rectangle {
    id: bar
    height: 30
    color: '#222'
    signal execute()

    RowLayout {
        anchors.fill: parent


        Rectangle {
            width: filebtntext.width*1.2
            height: parent.height
            color: '#eee'

            Text {
                anchors.centerIn: parent
                id: filebtntext
                text: 'execute'
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    bar.execute()
                }
            }
        }
    }
}
