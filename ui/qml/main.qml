import QtQuick
import QtQuick.Window
import QtQuick.Layouts

import "../js/nodes.js" as NodesScript
import "../js/painting.js" as PaintingScript

Item {
    width: 640
    height: 480
    visible: true

    Background {}

    ColumnLayout {
        anchors.fill: parent

        TopBar {
            Layout.preferredWidth: parent.width

            onExecute: {
                let commands = [];

                for(let i = 0; i < NodesScript.nodes.length; i++)
                {
                    commands.push(NodesScript.nodes[i].command_text);
                }

                backend.execute(commands, NodesScript.connections);
            }
        }

        spacing: 0

        Canvas {
            id: canvas
            Layout.fillHeight: true
            Layout.preferredWidth: parent.width
            Layout.minimumHeight: 40

            property real lastX
            property real lastY
            property alias paint_area: paint_area
            property MouseArea currentPort: null

            Component.onCompleted: {
                NodesScript.device_pixel_ratio = Screen.devicePixelRatio
            }

            onPaint: {
                PaintingScript.paint(canvas, NodesScript.nodes, NodesScript.connections)
            }

            MouseArea {
                id: paint_area
                hoverEnabled: true
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton


                onClicked: (mouse)=> {
                    if (mouse.button !== Qt.RightButton)
                    {
                       return
                    }

                    NodesScript.create_node(mouseX, mouseY)
                }

                onPressed: (mouse) => {
                    if(canvas.currentPort == null)
                    {
                       return
                    }

                    var port = canvas.currentPort
                    var coords = port.mapToItem(canvas, port.width/2, port.height/2)
                    canvas.lastX = coords.x
                    canvas.lastY = coords.y
                }

                onPositionChanged: {
                    if(canvas.port === null)
                    {
                        return
                    }

                    canvas.requestPaint()
                }

                onReleased: {
                    var coords = mapToGlobal(mouseX, mouseY)
                    NodesScript.connect_ports(canvas, coords, canvas.currentPort)

                    canvas.requestPaint()
                    canvas.currentPort = null
                }
            }
        }

    }
}
